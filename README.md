# Image Service

[Imagenary](https://github.com/h2non/imaginary) is used as image service
without any additional modification. This repository merely allows deploying
the service to staging and production.

Edit the `$UPSTREAM_IMAGE` in [.gitlab-ci.yml](.gitlab-ci.yml) to update.
